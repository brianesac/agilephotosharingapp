const mongoose = require('mongoose')
const connectDB = async () => {
    await mongoose.connect('mongodb://localhost/photoSharingDb', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    });
    mongoose.set('debug', true)
}
connectDB()

const Photo = mongoose.model('Photo', {
    id: Number,
    email: String,
    password: String
});

exports.postRegister = async function (request, h) {
    let maxId = await Photo.aggregate([{ $group: { _id: "id", max: { $max: "$id" } } }])
    let nextId = 0
    if (maxId === null || maxId === undefined) {
        nextId = 1
    }
    nextId = maxId[0].max + 1
    Object.assign(request.payload, { id: nextId})
    await Photo.insertMany([request.payload])
    return h.response(request.payload).code(201)
}
