'use strict';
const Joi = require('@hapi/joi')
const Hapi = require('@hapi/hapi')
const handler = require('./Handler/handler')
const init = async () => {
    const server = Hapi.server({
        port: 5678,
        host: '0.0.0.0',
    });

    server.route([
        {
            method: 'POST',
            path: '/register',
            handler: handler.postRegister,
            options:{
                validate: {
                    payload: {
                        id: Joi.forbidden(),
                        email: Joi.string().required(),
                        password: Joi.string().required()
                    }
                }
            }
        },
        {
            method: 'GET',
            path: '/login',
            handler: handler.login,
            options:{
                validate: {
                    payload: {
                        id: Joi.forbidden(),
                        email: Joi.string().required(),
                        password: Joi.string().required()
                    }
                }
            },
        },
    ]);
    server.start()
    console.log('Server running at: ', server.info.uri)
    return server
}
init()
module.exports = { init }