/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    StatusBar,
} from 'react-native';

import { styles } from './Style'

class LoginPage extends React.Component {

    state = {
        email: '',
        password: ''
    }

    renderLoginButton() {
        const buttonNotValid = <TouchableOpacity style={styles.registerButton} disabled={true}><Text style={{ alignSelf: 'center', marginVertical: '5%' }}>Your data still not valid guys</Text></TouchableOpacity>
        const buttonValid = <TouchableOpacity style={styles.registerButton2} onPress={() => this.userRegister()}><Text style={{ alignSelf: 'center', marginVertical: '5%' }}>Let's rock!</Text></TouchableOpacity>
        if ((this.state.password != this.state.reenterPassword) || (!this.state.email.match(/@RealUniversity.com\b/gi)) || (this.state.password === '' || this.state.reenterPassword === '' || this.state.email === '')) {
            return buttonNotValid
        }
        return buttonValid;
    }

    render() {
        return (
            <View style={styles.body}>
                <KeyboardAvoidingView behavior='position' contentContainerStyle={styles.textInputWrapper2} enabled>
                    <View style={styles.textInputWrapper}>
                        <Text style={styles.textSignUp}>Whassup guys!</Text>
                        <TextInput style={styles.textInput} placeholder='email' keyboardType='email-address' onChangeText={(email) => this.setState({ email })} value={this.state.email} clearTextOnFocus={true}></TextInput>
                        <TextInput style={styles.textInput} placeholder='password' secureTextEntry onChangeText={(password) => this.setState({ password })} value={this.state.password} clearTextOnFocus={true}></TextInput>
                        {this.renderLoginButton()}
                    </View>
                </KeyboardAvoidingView>
            </View>
        )
    }
}

export default LoginPage;
