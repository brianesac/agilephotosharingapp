/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';

import axios from 'axios'
import { styles } from './Style'

class RegisterPage extends React.Component {

  state = {
    email: '',
    password: '',
    reenterPassword: '',
    condition: true
  }

  async userRegister() {
    try {
      let postTask = await axios.post('http://20.4.12.206:5678/register', {
        email: this.state.email,
        password: this.state.password,

      })
      console.log(postTask)
      if (postTask) {
        alert('Good Job Guys')
        this.props.navigation.navigate('LoginPage')
      }

    }
    catch (err) {
      console.log(err)
    }
  }

  renderRegisterButton() {
    const buttonNotValid = <TouchableOpacity style={styles.registerButton} disabled={true}><Text style={{ alignSelf: 'center', marginVertical: '5%' }}>Your data still not valid guys</Text></TouchableOpacity>
    const buttonValid = <TouchableOpacity style={styles.registerButton2} onPress={() => this.userRegister()}><Text style={{ alignSelf: 'center', marginVertical: '5%' }}>Ok, Register Now</Text></TouchableOpacity>
    if ((this.state.password != this.state.reenterPassword) || (!this.state.email.match(/@RealUniversity.com\b/gi)) || (this.state.password === '' || this.state.reenterPassword === '' || this.state.email === '')) {
      return buttonNotValid
    }
    return buttonValid;
  }

  render() {
    return (
      <View style={styles.body}>
        <KeyboardAvoidingView behavior='position' contentContainerStyle={styles.textInputWrapper2} enabled>
          <View style={styles.textInputWrapper}>
            <Text style={styles.textSignUp}>C'mon Sign Up!</Text>
            <TextInput style={styles.textInput} placeholder='email' keyboardType='email-address' onChangeText={(email) => this.setState({ email })} value={this.state.email} clearTextOnFocus={true}></TextInput>
            <TextInput style={styles.textInput} placeholder='password' secureTextEntry onChangeText={(password) => this.setState({ password })} value={this.state.password} clearTextOnFocus={true}></TextInput>
            <TextInput style={styles.textInput} placeholder='re-enter password' secureTextEntry onChangeText={(reenterPassword) => this.setState({ reenterPassword })} value={this.state.reenterPassword} clearTextOnFocus={true}></TextInput>
            {this.renderRegisterButton()}
            <View style={styles.alreadyHaveAccount}>
              <Text>Or you already have an account?</Text><TouchableOpacity onPress={() => this.props.navigation.navigate('LoginPage')}><Text style={styles.okLoginText}>Ok, Login</Text></TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

export default RegisterPage;
