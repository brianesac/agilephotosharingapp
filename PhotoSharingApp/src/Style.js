/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
} from 'react-native';

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'skyblue',
        flex: 1
    },
    textInputWrapper: {
        marginHorizontal: '7%',
        marginTop: 100,
    },
    textInputWrapper2: {
        marginBottom: 30,
    },
    textInput: {
        borderWidth: 1,
        borderRadius: 30,
        textAlign: 'center',
        marginTop: 15,
        elevation: 10,
        backgroundColor: 'rgba(255,255,255,0.4)'
        // width: '90%',
    },
    registerButton: {
        borderWidth: 1,
        marginTop: 15,
        alignSelf: 'center',
        backgroundColor: '#f4f6f7',
        elevation: 10,
        width: '100%',
        borderRadius: 30
    },
    registerButton2: {
        borderWidth: 1,
        marginTop: 15,
        alignSelf: 'center',
        backgroundColor: 'skyblue',
        elevation: 10,
        width: '100%',
        borderRadius: 30
    },
    textSignUp: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 30
    },
    alreadyHaveAccount: {
        marginTop: 15,
        flexDirection: 'row', 
        justifyContent: 'space-around'
    },
    okLoginText:{
        color: '#7d3c98',
    }

})

export { styles }
