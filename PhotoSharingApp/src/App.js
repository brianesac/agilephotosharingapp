import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import RegisterPage from './RegisterPage'
import LoginPage from './LoginPage'

const MainNavigator = createStackNavigator({
  RegisterPage: {
    screen: RegisterPage,
    navigationOptions: () => ({
      headerShown: false,
    }),
  },
  LoginPage: {
    screen: LoginPage,
    navigationOptions: () => ({
      headerShown: false,
    }),
  }
});

const App = createAppContainer(MainNavigator);

export default App;